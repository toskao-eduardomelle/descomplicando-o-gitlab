# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab ao vivo na Twitch

### Day-1

```bash
- Entendemos o que é o Git
- Entendemos o que é Working Dir Index e HEAD
- Entendemos o que é o GitLab
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git
- Como criar uma Branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o Merge na Master/Main
- Como associar um repo local a um repo remoto
- Como importar um repo do GitHub para o GitLab
- Mudamos a Branch padrão para main
```
